#!/usr/bin/env python
import os
from BaseHTTPServer import HTTPServer
from SimpleHTTPServer import SimpleHTTPRequestHandler

ROUTES = [
	('/', '../Front')
]

class MyHandler(SimpleHTTPRequestHandler):
	
	def translate_path(self, path):
		# default root -> cwd        
		root = os.getcwd()

		# look up routes and get root directory
		for patt, rootDir in ROUTES:
			print 'patt: {}, raiz: {}'.format(patt, rootDir)
			print 'Caminho: {}'.format(path)
			if path.startswith(patt):                
				path = path[len(patt):]
				root = rootDir
				break
		# new path
		print 'join: {}'.format(os.path.join(root, path))
		return os.path.join(root, path)    

if __name__ == '__main__':
	httpd = HTTPServer(('localhost', 8000), MyHandler)
	httpd.serve_forever()