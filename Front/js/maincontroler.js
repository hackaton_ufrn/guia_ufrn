/* variaveis para controlar a criacao dos dropdowns */
var drop1;
var drop2;
var drop3;

$(document).ready(function(){
    console.log("page ready");
    
    //alterando as variaveis de criação dos dropdowns
    drop1 = false;
    drop2 = false;
    drop3 = false;
    
    //desativando drop2 e drop3
    $('#area_do_conhecimento').hide()
    $('#linha_de_pesquisa').hide();
    
});

//url do servidor que vamos usar
var url_ = "http://localhost:1337";

//Função que trata o click no botão do dropdown de Unidades
function dropBtnUnidades(){
    //ação executada pelo botão
    var action = "/user";
    //configuração e chamada ao servidor
    $.ajax({
        url: url_ + action,
        type: "GET",
        dataType: "json",
        
        xhrFields: {
    // The 'xhrFields' property sets additional fields on the XMLHttpRequest.
    // This can be used to set the 'withCredentials' property.
    // Set the value to 'true' if you'd like to pass cookies to the server.
    // If this is enabled, your server must respond with the header
    // 'Access-Control-Allow-Credentials: true'.
    withCredentials: false
  },

  headers: {
    // Set any custom headers here.
    // If you set any non-simple headers, your server must include these
    // headers in the 'Access-Control-Allow-Headers' response header.
  },
    })
    .done(function( json ){
        if(!drop1){
            for(var i=0; i<json.length; i++){
                var li = document.createElement("li");
                var a = document.createElement("a");
                var text = document.createTextNode(json[i].username);
                a.setAttribute("href", "#");
                a.addEventListener("click", function(e){
                    if(!drop2){
                        $('#area_do_conhecimento').show();
                        dropBtnAreadoConhecimento();
                    }
                }, true);
                a.appendChild(text);
                li.appendChild(a);
                document.getElementById('drop_unidades').appendChild(li);
            }
            drop1 = !drop1;
        }
    })
    .fail(function( json ){
        console.log(json);
    })
    .always(function(){
        console.log("request completed");
    })
}


function dropBtnAreadoConhecimento(){
    //ação executada pelo botão
    var action = "/user";
    //configuração e chamada ao servidor
    $.ajax({
        url: url_ + action,
        type: "GET",
        dataType: "json",
        
        xhrFields: {
    // The 'xhrFields' property sets additional fields on the XMLHttpRequest.
    // This can be used to set the 'withCredentials' property.
    // Set the value to 'true' if you'd like to pass cookies to the server.
    // If this is enabled, your server must respond with the header
    // 'Access-Control-Allow-Credentials: true'.
    withCredentials: false
  },

  headers: {
    // Set any custom headers here.
    // If you set any non-simple headers, your server must include these
    // headers in the 'Access-Control-Allow-Headers' response header.
  },
    })
    .done(function( json ){
        if(!drop2){
            for(var i=0; i<json.length; i++){
                var li = document.createElement("li");
                var a = document.createElement("a");
                var text = document.createTextNode(json[i].username);
                a.setAttribute("href", "#");
                a.addEventListener("click", function(e){
                    if(!drop3){
                        $('#linha_de_pesquisa').show();
                        dropBtnLinhadePesquisa();   
                    }
                }, true);
                a.appendChild(text);
                li.appendChild(a);
                document.getElementById('drop_areadoconhecimento').appendChild(li);
            }
            drop2 = !drop2;
        }
    })
    .fail(function( json ){
        console.log(json);
    })
    .always(function(){
        console.log("request completed");
    })
}


//Função que trata o click no botão do dropdown da Linha de Pesquisa
function dropBtnLinhadePesquisa(){
    //ação executada pelo botão
    var action = "/user";
    //configuração e chamada ao servidor
    $.ajax({
        url: url_ + action,
        type: "GET",
        dataType: "json",
        
        xhrFields: {
    // The 'xhrFields' property sets additional fields on the XMLHttpRequest.
    // This can be used to set the 'withCredentials' property.
    // Set the value to 'true' if you'd like to pass cookies to the server.
    // If this is enabled, your server must respond with the header
    // 'Access-Control-Allow-Credentials: true'.
    withCredentials: false
  },

  headers: {
    // Set any custom headers here.
    // If you set any non-simple headers, your server must include these
    // headers in the 'Access-Control-Allow-Headers' response header.
  },
    })
    .done(function( json ){
        if(!drop3){
            for(var i=0; i<json.length; i++){
                var li = document.createElement("li");
                var a = document.createElement("a");
                var text = document.createTextNode(json[i].username);
                a.setAttribute("href", "#");
                a.addEventListener("click", function(e){
                    alert("deu certo " + e.target.text);
                }, true);
                a.appendChild(text);
                li.appendChild(a);
                document.getElementById('drop_linhadepesquisa').appendChild(li);
            }
            drop3 = !drop3;
        }
    })
    .fail(function( json ){
        console.log(json);
    })
    .always(function(){
        console.log("request completed");
    })
}
